//
//  NCGR_IOS_MobileTaskTests.swift
//  NCGR_IOS_MobileTaskTests
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import XCTest
@testable import NCGR_IOS_MobileTask
import RxSwift


class NCGR_IOS_MobileTaskTests: XCTestCase {
    
    
    let disposeBag = DisposeBag()
    let cityId = 1521894
    let repo = CityDetailsReprository(remoteDataSource: RemoteCityDetailsDataSource(), cachedDataSource: RemoteCityDetailsDataSource())
    lazy var  viewModel: CityDetailsViewModel = CityDetailsViewModel(repo: repo, cityId: self.cityId)
    let remoteCityDetailsDataSource = RemoteCityDetailsDataSource()
    override func setUpWithError() throws {
        
    }
    
    override func tearDownWithError() throws {
        super.tearDown()
    }
    
    func testValidateNetWorkCallsForVenue() throws {
        let promise = expectation(description: "api not returned successsed")
        NCGREndPoints.shared
            .provider.rx
            .request(.cityDays(cityId: self.cityId))
            .filterSuccessfulStatusCodes()
            .timeout(.seconds(60), scheduler: MainScheduler.instance)
            .retry(2)
            .map(CityDetailsModel.self)
            .subscribe(onSuccess: { response in
                promise.fulfill()
            }) { error in
              
                
            }.disposed(by: self.disposeBag)
        wait(for: [promise], timeout: 60)
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
