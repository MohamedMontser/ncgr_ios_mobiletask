//
//  HebatEndpointSpecifications.swift
//  Hebat
//
//  Created by mohamed hashem on 11/17/20.
//  Copyright © 2020 mohamed hashem. All rights reserved.
//

import Foundation
import UIKit
import Moya
import Photos

// MARK: - Provider Specifications
enum NCGREndpointSpecifications {
    case cityDays(cityId: Int)
    case addWEIDS(cityName: String)
}

// MARK: - Provider release url
let releaseURL = "https://www.metaweather.com/api/"
let BaseImgUrl = "https://www.metaweather.com/"
let DEVELOPMENT_URL = "https://www.metaweatherDevelopment.com/api/"
let PRODUCTION_URL = "https://www.metaweatherProduction.com/api/"


// MARK: - Provider target type
extension NCGREndpointSpecifications: TargetType {
    
    var baseURL: URL {
        switch self {
        default:
            #if DEVELOPMENT
            return URL(string: DEVELOPMENT_URL)!
            #elseif PRODUCTION
            return URL(string: PRODUCTION_URL)!
            #else
            return URL(string: releaseURL)!
            #endif
        }
    }

    var path: String {
        switch self {
        case .cityDays(let cityId):
            return "location/\(cityId)"
        case .addWEIDS(let cityName):
            return "location/search/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .cityDays, .addWEIDS:
            return .get
        }
    }

    var headers: [String : String]? {
        switch self {
        case .cityDays, .addWEIDS:
            return  ["Accept-Language" : "en"]
        }
    }

    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)! as Data
    }

    var task: Task {
        switch self {
        
        case .cityDays:
            return .requestPlain
        case .addWEIDS(let cityName):
            return .requestParameters(parameters: ["query" : cityName], encoding:  URLEncoding.queryString)
        }
    }
}


// MARK: - Helpers
private extension String {
    var urlEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8EncodedData: Data {
        return self.data(using: .utf8)!
    }
}
