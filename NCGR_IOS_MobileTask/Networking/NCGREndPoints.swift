//
//  HebatEndPoints.swift
//  Hebat
//
//  Created by mohamed hashem on 11/17/20.
//  Copyright © 2020 mohamed hashem. All rights reserved.
//

import Foundation
import Moya
import RxSwift

// MARK: - Provider support
final class NCGREndPoints {

    static var shared = NCGREndPoints()

    let provider = MoyaProvider<NCGREndpointSpecifications>(plugins: [AccessTokenPlugin { _ in
                                                                        ConstantStore
                                                                            .sharedInstance
                                                                            .authenticationKeyForClient
                                                                            .count > 1 ? ConstantStore
                                                                            .sharedInstance.authenticationKeyForClient : "" },
                                                                       CompleteUrlLoggerPlugin()])
}

class CompleteUrlLoggerPlugin : PluginType {
    func willSend(_ request: RequestType, target: TargetType) {
        //for test
     }
}
