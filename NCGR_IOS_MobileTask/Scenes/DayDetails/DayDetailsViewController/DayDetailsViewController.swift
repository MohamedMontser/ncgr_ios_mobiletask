//
//  DayDetailsViewController.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/16/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import UIKit

class DayDetailsViewController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var weatherStateNameLbl: UILabel!
    @IBOutlet weak var maxLbl: UILabel!
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var mphLBL: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var HumidityLbl: UILabel!
    @IBOutlet weak var VisibilityLbl: UILabel!
    @IBOutlet weak var PressureLbl: UILabel!
    @IBOutlet weak var ConfidenceLbl: UILabel!
    
    private var consolidatedWeather: ConsolidatedWeatherUIModel!
    private  let controllerNibName = "DayDetailsViewController"
    private var cityName: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        title = self.cityName
        setDayDetailsData()
    }
    
    init(consolidatedWeather: ConsolidatedWeatherUIModel, cityName: String) {
        self.cityName = cityName
        self.consolidatedWeather = consolidatedWeather
        super.init(nibName: controllerNibName, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setDayDetailsData() {
        self.titleLbl.text = consolidatedWeather.dayName
        self.weatherStateNameLbl.text = consolidatedWeather.weatherStateName?.rawValue
        self.maxLbl.text = consolidatedWeather.maxTemp
        self.minLbl.text = consolidatedWeather.minTemp
        self.mphLBL.text = consolidatedWeather.windSpeed
        self.imgView.sd_setImage(with: URL(string: consolidatedWeather.weatherStateIconURL), placeholderImage: UIImage(named: ""))
        self.HumidityLbl.text = consolidatedWeather.humidity
        self.ConfidenceLbl.text = consolidatedWeather.predictability
        self.VisibilityLbl.text = consolidatedWeather.visibility
        self.PressureLbl.text = consolidatedWeather.airPressure
    }
}
