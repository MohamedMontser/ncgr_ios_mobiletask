//
//  Weather States.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/16/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import Foundation


enum WeatherStates: String, Codable {
    case Snow = "Snow"
    case Sleet = "Sleet"
    case Hail = "Hail"
    case Thunderstorm = "Thunderstorm"
    case HeavyRain = "Heavy Rain"
    case LightRain = "Light Rain"
    case Showers = "Showers"
    case HeavyCloud = "Heavy Cloud"
    case LightCloud = "Light Cloud"
    case Clear = "Clear"
}
