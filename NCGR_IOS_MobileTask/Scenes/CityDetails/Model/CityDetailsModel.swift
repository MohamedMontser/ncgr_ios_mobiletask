//
//  CityDetailsModel.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import Foundation


struct CityDetailsUIModel {
    let consolidatedWeather: [ConsolidatedWeatherUIModel]!
    let time, sunRise, sunSet, timezoneName: String!
    let parent: ParentUIModel!
    let sources: [SourceUIModel]!
    let title, locationType: String!
    let woeid: Int!
    let lattLong, timezone: String!
    
    init(cityDetailsModel: CityDetailsModel){
        self.consolidatedWeather = cityDetailsModel.consolidatedWeather?.map({ConsolidatedWeatherUIModel(ConsolidatedWeather: $0)})
        self.time = cityDetailsModel.time
        self.woeid = cityDetailsModel.woeid
        self.title = cityDetailsModel.title
        self.lattLong = cityDetailsModel.lattLong
        self.timezone = cityDetailsModel.timezone
        self.parent = ParentUIModel(parent: cityDetailsModel.parent)
        self.sunRise = cityDetailsModel.sunRise
        self.sunSet = cityDetailsModel.sunSet
        self.locationType = cityDetailsModel.locationType
        self.timezoneName = cityDetailsModel.timezoneName
        self.sources = cityDetailsModel.sources?.map({SourceUIModel(source: $0)})
    }
    
    init?(){
        return nil
    }
}


// MARK: - CityModel
struct CityDetailsModel:  Encodable, Decodable {
    
    
    let consolidatedWeather: [ConsolidatedWeather]?
    let time, sunRise, sunSet, timezoneName: String?
    let parent: Parent?
    let sources: [Source]?
    let title, locationType: String?
    let woeid: Int?
    let lattLong, timezone: String?
    
    enum CodingKeys: String, CodingKey {
        case consolidatedWeather = "consolidated_weather"
        case time
        case sunRise = "sun_rise"
        case sunSet = "sun_set"
        case timezoneName = "timezone_name"
        case parent, title
        case locationType = "location_type"
        case woeid
        case lattLong = "latt_long"
        case timezone
        case sources
    }
    
    init?(){
        return nil
    }
}

struct ConsolidatedWeatherUIModel {
    let id: Int!
    let  weatherStateAbbr, windDirectionCompass, created: String!
    let weatherStateName: WeatherStates!
    let applicableDate: String!
    let minTemp, maxTemp, theTemp, windSpeed: String?
    let windDirection: Double!
    let humidity ,airPressure: String!
    let visibility: String!
    let predictability: String!
    
    var weatherStateIconURL: String!{
        return BaseImgUrl + "static/img/weather/png/" + (self.weatherStateAbbr ?? "") + ".png"
    }
    
    var dayName: String{
        return Helper.getValidDateString(dateString: self.applicableDate ?? "")
    }
    
    
    init(ConsolidatedWeather: ConsolidatedWeather?) {
        self.id = ConsolidatedWeather?.id
        self.weatherStateAbbr = ConsolidatedWeather?.weatherStateAbbr
        self.windDirectionCompass = ConsolidatedWeather?.windDirectionCompass
        self.created = ConsolidatedWeather?.created
        self.weatherStateName = ConsolidatedWeather?.weatherStateName
        self.applicableDate = ConsolidatedWeather?.applicableDate
        self.minTemp =  "Max: \(String(Int(ConsolidatedWeather?.minTemp ?? 0)))°C"
        self.maxTemp = "Min: \(String(Int(ConsolidatedWeather?.maxTemp ?? 0)))°C"
        self.theTemp = String(Int(ConsolidatedWeather?.theTemp ?? 0) )
        self.windSpeed = String(Int(ConsolidatedWeather?.windSpeed ?? 0)) + "mph"
        self.windDirection = ConsolidatedWeather?.windDirection
        self.visibility = String(format: "\(ConsolidatedWeather?.visibility ?? 0)", "0.1f") + " miles"
        self.airPressure = String(Int(ConsolidatedWeather?.airPressure ?? 0)) + "mb"
        self.humidity = "\(ConsolidatedWeather?.humidity ?? 0)%"
        self.predictability = "\(ConsolidatedWeather?.predictability ?? 0)%"
    }
}

// MARK: - ConsolidatedWeather
struct ConsolidatedWeather: Codable {
    let id: Int?
    let  weatherStateAbbr, windDirectionCompass, created: String?
    let weatherStateName: WeatherStates?
    let applicableDate: String?
    let minTemp, maxTemp, theTemp, windSpeed: Double?
    let windDirection, airPressure: Double?
    let humidity: Int?
    let visibility: Double?
    let predictability: Int?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case weatherStateName = "weather_state_name"
        case weatherStateAbbr = "weather_state_abbr"
        case windDirectionCompass = "wind_direction_compass"
        case created
        case applicableDate = "applicable_date"
        case minTemp = "min_temp"
        case maxTemp = "max_temp"
        case theTemp = "the_temp"
        case windSpeed = "wind_speed"
        case windDirection = "wind_direction"
        case airPressure = "air_pressure"
        case humidity, visibility, predictability
    }
    
    init?(){
        return nil
    }
}

struct ParentUIModel {
    let title, locationType: String
    let woeid: Int
    let lattLong: String
    
    init(parent: Parent?) {
        self.title = parent?.title ?? ""
        self.locationType = parent?.locationType ?? ""
        self.woeid = parent?.woeid ?? 0
        self.lattLong = parent?.lattLong ?? ""
    }
}

// MARK: - Parent
struct Parent: Codable {
    let title, locationType: String?
    let woeid: Int?
    let lattLong: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case locationType = "location_type"
        case woeid
        case lattLong = "latt_long"
    }
}

struct SourceUIModel{
    let title, slug: String
    let url: String
    let crawlRate: Int
    
    init(source: Source) {
        self.title = source.title ?? ""
        self.url = source.url ?? ""
        self.crawlRate = source.crawlRate ?? 0
        self.slug = source.slug ?? ""
    }
}

// MARK: - Source
struct Source: Codable {
    let title, slug: String?
    let url: String?
    let crawlRate: Int?
    
    enum CodingKeys: String, CodingKey {
        case title, slug, url
        case crawlRate = "crawl_rate"
    }
}
