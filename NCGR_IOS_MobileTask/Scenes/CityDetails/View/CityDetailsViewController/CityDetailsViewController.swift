//
//  CityDetailsViewController.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class CityDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel: CityDetailsViewModel
    private let disposeBag = DisposeBag()
    
    let controllerNibName = "CityDetailsViewController"
    let dayTableViewCell = "DayTableViewCell"
    var cityDetailsModel: CityDetailsUIModel?
    let cityModel: CityModel
    
    init(cityModel: CityModel) {
        let repo = CityDetailsReprository(remoteDataSource: RemoteCityDetailsDataSource(), cachedDataSource: RemoteCityDetailsDataSource())
        self.cityModel = cityModel
        viewModel = CityDetailsViewModel(repo: repo, cityId: cityModel.id)
        super.init(nibName: controllerNibName, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        title = self.cityModel.name
        self.tableView.register(UINib(nibName: dayTableViewCell, bundle: nil), forCellReuseIdentifier: dayTableViewCell)
        bindData()
        sendRequetBasedOnReachability()
    }
    
    private func sendRequetBasedOnReachability() {
        InternetConnection.shared.startListening() { [weak self] connectionStatus in
            guard let self = self else { return }
            switch connectionStatus {
            case .connected:
                self.viewModel.getCityDetails(isConnected: true)
            case .disConnected:
                self.viewModel.getCityDetails(isConnected: false)
                print("dis")
            }
        }
    }
    
    func bindData() {
        viewModel.output.reloadData.subscribeOn(MainScheduler.asyncInstance).bind { () in
            self.tableView.reloadData()
        }.disposed(by: disposeBag)
        viewModel.output.cashedError.subscribe { (error) in
            PopUpAlert.showErrorToastWith(message: error, nil)
        }.disposed(by: disposeBag)
        
        viewModel.output.error.subscribe { (error) in
            PKHUDIndicator.hideByErrorFlash()
            PopUpAlert.showErrorToastWith(error)
        } onError: { (error) in
            PKHUDIndicator.hideByErrorFlash()
            PopUpAlert.showErrorToastWith(error)
        }.disposed(by: disposeBag)
    }
}

//MARK:- table view data source, table view delegate
extension CityDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.cityDetailsModel?.consolidatedWeather?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DayTableViewCell") as! DayTableViewCell
        if  let consolidatedWeather = self.viewModel.cityDetailsModel?.consolidatedWeather[indexPath.row] {
            cell.configurCell(consolidatedWeather: consolidatedWeather)}
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  let consolidatedWeather = self.viewModel.cityDetailsModel?.consolidatedWeather[indexPath.row] {
            let vc = DayDetailsViewController(consolidatedWeather: consolidatedWeather, cityName: viewModel.cityDetailsModel?.title ?? "")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
