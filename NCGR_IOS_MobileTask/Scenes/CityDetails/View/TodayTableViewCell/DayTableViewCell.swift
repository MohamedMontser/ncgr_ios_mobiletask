//
//  DayTableViewCell.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import UIKit
import SDWebImage

class DayTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var weatherStateNameLbl: UILabel!
    @IBOutlet weak var maxLbl: UILabel!
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var mphLBL: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    func configurCell(consolidatedWeather: ConsolidatedWeatherUIModel){
        self.titleLbl.text = consolidatedWeather.dayName
        self.weatherStateNameLbl.text = consolidatedWeather.weatherStateName?.rawValue
        self.maxLbl.text = consolidatedWeather.maxTemp
        self.minLbl.text = consolidatedWeather.minTemp
        self.mphLBL.text = consolidatedWeather.windSpeed
        self.imgView.sd_setImage(with: URL(string: consolidatedWeather.weatherStateIconURL), placeholderImage: UIImage(named: ""))
    }
}
    
