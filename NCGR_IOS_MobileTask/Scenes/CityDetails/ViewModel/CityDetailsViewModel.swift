//
//  CityDetailsViewModel.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import Foundation
import RxSwift
import DataCache


protocol CityDetailsDataRepositry {
    func getCityDetails(with cityID: Int) -> Observable<CityDetailsModel>
}

protocol CityDetailsDataSource {
    func getCityDetails(with cityID: Int) -> Observable<CityDetailsModel>
}

class RemoteCityDetailsDataSource: CityDetailsDataSource {

    func getCityDetails(with cityID: Int) -> Observable<CityDetailsModel> {
        return Observable.create { [weak self] observer in
            guard self != nil else {
                return Disposables.create {}
            }
            return NCGREndPoints.shared
                .provider.rx
                .request(.cityDays(cityId: cityID))
                .filterSuccessfulStatusCodes()
                .timeout(.seconds(60), scheduler: MainScheduler.instance)
                .retry(2)
                .map(CityDetailsModel.self)
                .subscribe(onSuccess: { response in
                    CashedHandler.cacheCityDetailsModel(forCityID: cityID, citydetailsModel: response)
                    observer.onNext(response)
                    print("data\(response)")
                }) { [weak self] error in
                    if  let cached = CashedHandler.getCityDetailsModelFromCached(forCityID: cityID)  {
                        observer.onNext(cached)
                    }else{
                        observer.onError(error)
                    }
                }
        }
    }
}

class CityDetailsReprository: CityDetailsDataRepositry {
  
    private let remoteDataSource: CityDetailsDataSource
        
    init(remoteDataSource: CityDetailsDataSource, cachedDataSource: CityDetailsDataSource) {
        self.remoteDataSource = remoteDataSource
    }
    
    func getCityDetails(with cityID: Int) -> Observable<CityDetailsModel> {
            return  remoteDataSource.getCityDetails(with: cityID)
    }
}


final class CityDetailsViewModel {
    
    struct Output {
        let reloadData: Observable<Void>
        let error: Observable<Error>
        let cashedError: Observable<String>
    }
    
    private(set) var cityDetailsModel: CityDetailsUIModel?
    private let repo: CityDetailsDataRepositry
    private let cityId: Int
    private let reloadDataSubject = PublishSubject<Void>()
    private let  errorSubject =  PublishSubject<Error>()
    private let  cashedErrorSubject =  PublishSubject<String>()
    private let disposed = DisposeBag()
    
    let output: Output
        
    init(repo: CityDetailsDataRepositry, cityId: Int) {
        output = Output(reloadData: reloadDataSubject.asObservable(), error: errorSubject.asObservable(), cashedError: cashedErrorSubject.asObserver())
        self.repo = repo
        self.cityId = cityId
    }
    
    func getCityDetails(isConnected: Bool) {
        if !isConnected {
            let cityDetails = CashedHandler.getCityDetailsModelFromCached(forCityID: cityId)
            if let cityDetails = cityDetails{
                self.cityDetailsModel = CityDetailsUIModel(cityDetailsModel: cityDetails)
            }else {
                cashedErrorSubject.onNext("There isn't data for that")
            }
            self.reloadDataSubject.onNext(())
        } else {
            repo.getCityDetails(with: cityId).subscribe { [weak self] (cityDetails)  in
                self?.cityDetailsModel = CityDetailsUIModel(cityDetailsModel: cityDetails)
                self?.reloadDataSubject.onNext(())
            } onError: { [weak self] (error) in
                self?.errorSubject.onNext(error)
            }.disposed(by: disposed)
        }
    }
}
