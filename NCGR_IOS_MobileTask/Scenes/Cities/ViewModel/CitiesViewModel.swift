
//
//  CityDetailsViewModel.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import Foundation
import RxSwift
import DataCache


protocol CitieDataSource {
    func getCities() ->  Observable<[CityModel]>
    func addWIED(cityName: String) -> Observable<AddwoeidModel>
}


class CitiesSource: CitieDataSource {
    func addWIED(cityName: String) -> Observable<AddwoeidModel> {
        return Observable.create { [weak self] observer in
            guard self != nil else {
                return Disposables.create {}
            }
            return NCGREndPoints.shared
                .provider.rx
                .request(.addWEIDS(cityName: cityName))
                .filterSuccessfulStatusCodes()
                .timeout(.seconds(60), scheduler: MainScheduler.instance)
                .retry(2)
                .map([AddwoeidModel].self)
                .subscribe(onSuccess: { response in
                    CashedHandler.cacheCityModel(addWoiedModel: response.first)
                    guard let firstAddWoeid = response.first else {return}
                    observer.onNext(firstAddWoeid)
                }) { error in
                    observer.onError(error)
                }
        }
    }
    
    func getCities() -> Observable<[CityModel]>{
        let cachedCities = CashedHandler.getCityModelFromCached()
        var cities = [CityModel(id: 1521894, name: "Cairo"), CityModel(id: 2459115, name: "NY"), CityModel(id: 1939753, name: "Riyadh")]
        if let cachedCities = cachedCities {
            cities.append(contentsOf: cachedCities)
        }
        return Observable.of(cities)
    }
}



final class CitiesViewModel {
    
    struct Output {
        let cities: Observable<[CityModel]>
        let FailedToAddWEID: Observable<String>
        let succeededToAddWEID: Observable<String>
        let error: Observable<Error>
    }
    
    private(set) var cityDetailsModel: CityDetailsUIModel?
    private let citiesDataSource: CitieDataSource
    private let citiesSubject = PublishSubject<[CityModel]>()
    private let  succeededToAddWEIDSubject =  PublishSubject<String>()
    private let  FailedToAddWEIDSubject =  PublishSubject<String>()
    private let  errorSubject =  PublishSubject<Error>()
    private let disposed = DisposeBag()
    
    let output: Output
    
    init(citiesDataSource: CitieDataSource) {
        output = Output(cities: citiesSubject.asObserver(), FailedToAddWEID: FailedToAddWEIDSubject.asObserver(), succeededToAddWEID: succeededToAddWEIDSubject.asObserver(), error: errorSubject.asObserver())
        self.citiesDataSource = citiesDataSource
    }
    
    func getCities() {
        citiesDataSource.getCities().subscribe { [weak self] (cities)  in
            self?.citiesSubject.onNext(cities)
        } onError: { [weak self] (error) in
            self?.errorSubject.onNext(error)
        }.disposed(by: disposed)
    }
    
    
    func addWEIOD(cityName: String, isConnected: Bool ) {
        
        if isConnected {
            self.FailedToAddWEIDSubject.onNext("Not connected")
        }
        citiesDataSource.addWIED(cityName: cityName).subscribe  { [weak self] (addwoeidModel)  in
            self?.getCities()
        } onError: { [weak self] (error) in
            self?.FailedToAddWEIDSubject.onNext(error.localizedDescription)
        }.disposed(by: disposed)
    }
}
