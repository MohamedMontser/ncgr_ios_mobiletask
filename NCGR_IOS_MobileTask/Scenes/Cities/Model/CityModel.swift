//
//  CityModel.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import Foundation

struct CityModel: Codable {
    var id: Int
    var name: String
}
