//
//  AddCityNameView.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/16/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import UIKit

class AddCityNameView: UIView {

    @IBOutlet weak var txtField: UITextField!
    
    var cityName: ((String) -> ())?
    var removeView:  (() -> ())?
    static func getInstance() -> AddCityNameView{
        let view = Bundle.main.loadNibNamed("AddCityNameView", owner: self, options: nil)?.first as! AddCityNameView
        return view
    }
    
    
    @IBAction func addWOIED() {
        cityName?(self.txtField.text ?? "")
        self.txtField.text?.removeAll()
    }
    
    @IBAction func removeWied() {
        self.txtField.text?.removeAll()
        removeView?()
    }

}
