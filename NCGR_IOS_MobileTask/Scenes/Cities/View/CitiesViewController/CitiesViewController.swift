//
//  CityDetailsViewController.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/15/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class CitiesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel: CitiesViewModel
    private let disposeBag = DisposeBag()
   
    let controllerNibName = "CitiesViewController"
    let cityTableViewCell = "CityTableViewCell"
    let AddCityView = AddCityNameView.getInstance()
    let viewTag = 333
    var cities: [CityModel]?
    
    init() {
        viewModel = CitiesViewModel(citiesDataSource: CitiesSource())
        super.init(nibName: controllerNibName, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        title = "Cities"
        self.addNavigationBarRightItem()
        self.tableView.register(UINib(nibName: cityTableViewCell, bundle: nil), forCellReuseIdentifier: cityTableViewCell)
        bindData()
        self.viewModel.getCities()
        handleAddCityNameViewsAction()
        
    }
    
    func addNavigationBarRightItem(){
        let addWOEID = UIBarButtonItem(title: "Add City", style: .done, target: self, action: #selector(handleAddWOEID))
        self.navigationItem.rightBarButtonItem  = addWOEID
    }
    
    func handleAddCityNameViewsAction() {
        self.AddCityView.removeView = { [weak self] in
            self?.removeddCityNameView()
        }
        self.AddCityView.cityName = { [weak self] cityName in
            guard let self = self else {return }
            self.removeddCityNameView()
            if self.checkOnCitiesName(cityName: cityName){
                PopUpAlert.showErrorToastWith(message: "This city already added before", nil)
            }else{
                self.sendRequetBasedOnReachability(withCityName: cityName)
            }
        }
    }
    
    
    func checkOnCitiesName(cityName: String) -> Bool{
        return self.cities?.filter({$0.name == cityName}).count ?? 0 > 0
    }
    func removeddCityNameView(){
        if let cityNameView = view.viewWithTag(viewTag) {
            cityNameView.removeFromSuperview()
        }
    }
    
    @objc func handleAddWOEID(){
        self.AddCityView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.AddCityView.tag = viewTag
        self.view.addSubview(self.AddCityView)
    }
    
    func sendRequetBasedOnReachability(withCityName name: String) {
        InternetConnection.shared.startListening() { [weak self] connectionStatus in
            guard let self = self else { return }
            switch connectionStatus {
            case .connected:self.viewModel.addWEIOD(cityName: name, isConnected: true)
            case .disConnected: self.viewModel.addWEIOD(cityName: name, isConnected: false)
            }
        }
    }
    
    func bindData() {
        viewModel.output.cities.subscribe { (cities) in
            self.cities = cities
            self.tableView.reloadData()
        } onError: { (error) in
            PopUpAlert.showErrorToastWith(error)
        }.disposed(by: disposeBag)
        
        viewModel.output.FailedToAddWEID.subscribe { (error) in
            PopUpAlert.showErrorToastWith(message: error, nil)
        }.disposed(by: disposeBag)

        viewModel.output.error.subscribe { (error) in
            PKHUDIndicator.hideByErrorFlash()
            PopUpAlert.showErrorToastWith(error)
        } onError: { (error) in
            PKHUDIndicator.hideByErrorFlash()
            PopUpAlert.showErrorToastWith(error)
        }.disposed(by: disposeBag)
    }
}

//MARK:- table view data source, table view delegate
extension CitiesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cityTableViewCell) as! CityTableViewCell
        cell.configureCell(city: self.cities![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  let city = self.cities?[indexPath.row] {
            let vc = CityDetailsViewController(cityModel: city)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
