//
//  CityTableViewCell.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/16/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
  
    @IBOutlet weak var cityNameLbl: UILabel!
    
    func configureCell(city: CityModel){
        self.cityNameLbl.text  = city.name
    }
    
}
