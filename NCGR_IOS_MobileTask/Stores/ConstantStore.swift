//
//  ConstantStore.swift
//  Hebat
//
//  Created by mohamed hashem on 19/11/2020.
//  Copyright © 2020 mohamed hashem. All rights reserved.
//

import Foundation
import RxSwift

class ConstantStore {
    
    static let sharedInstance = ConstantStore()
    private let disposed = DisposeBag()
    
    enum Features: String {
        case authenticationKey
        case currentUser
        case currentUserImage
        case categoryChosen
    }
    
    
    var isClientRegister: Bool {
        if authenticationKeyForClient.count > 4 {
            return true
        } else {
            return false
        }
    }
    
    var authenticationKeyForClient: String {
        set {
            UserDefaults.standard.set(newValue, forKey: Features.authenticationKey.rawValue)
            UserDefaults.standard.synchronize()
        }
        
        get {
            guard let authenticationKey = UserDefaults.standard.object(forKey: Features.authenticationKey.rawValue) as? String else {
                self.authenticationKeyForClient = ""
                return ""
            }
            return authenticationKey
        }
    }
    
    var currentUser: Data? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: Features.currentUser.rawValue)
            UserDefaults.standard.synchronize()
        }
        
        get {
            let object = UserDefaults.standard.data(forKey: Features.currentUser.rawValue)
            return object
        }
    }
    
    var currentUserImage: Data? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: Features.currentUserImage.rawValue)
            UserDefaults.standard.synchronize()
        }
        
        get {
            let object = UserDefaults.standard.data(forKey: Features.currentUserImage.rawValue)
            return object
        }
    }
}
