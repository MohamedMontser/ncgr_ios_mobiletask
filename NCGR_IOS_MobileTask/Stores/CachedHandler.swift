//
//  CachedHandler.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/16/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import Foundation
import DataCache


class CashedHandler {
    
    static func cacheCityDetailsModel(forCityID id: Int, citydetailsModel: CityDetailsModel) {
        do {
            print(citydetailsModel)
            try DataCache.instance.write(codable: citydetailsModel, forKey: "\(id)")
        } catch {
            print("Write error \(error.localizedDescription)")
        }
    }
    
    static func cacheCityModel(addWoiedModel: AddwoeidModel?) {
        do {
            var cityModels: [CityModel] = getCityModelFromCached() ?? []
            cityModels.append(CityModel(id: addWoiedModel?.woeid ?? 0, name: addWoiedModel?.title ?? ""))
            
            try DataCache.instance.write(codable: cityModels, forKey: "Cities")
        } catch {
            print("Write error \(error.localizedDescription)")
        }
    }
    
    
    
    static func getCityModelFromCached() -> [CityModel]? {
        do {
            let cityDetailsModel: [CityModel]? = try DataCache.instance.readCodable(forKey: "Cities")
            return cityDetailsModel
        } catch {
            return nil
        }
    }
    
    static func getCityDetailsModelFromCached(forCityID id: Int) -> CityDetailsModel? {
        do {
            let cityDetailsModel: CityDetailsModel? = try DataCache.instance.readCodable(forKey: "\(id)")
            return cityDetailsModel
        } catch {
            return nil
        }
    }
}
