//
//  UIView+Animation.swift
//  CountryFood
//
//  Created by mohamed hashem on 6/29/20.
//  Copyright © 2020 mohamed hashem. All rights reserved.
//

import SwiftMessages
import Network

protocol CheckInternetConnection: class {
    var reachabilityManager: connectionStatus { get }
}

class InternetConnection {
    
    static let shared = InternetConnection()
    let lessInternetConnectionMessage = SwiftMessages()
    
    func startListening(_ reachableClosure:@escaping (connectionStatus) -> () = { _ in }) {
        
        var config = SwiftMessages.defaultConfig
        
        config.duration = .forever
        config.interactiveHide = false
        config.ignoreDuplicates = false
        config.presentationStyle = .bottom
        
        guard let view: NoInternetConnectionMessageView = try? SwiftMessages.viewFromNib() else {
            fatalError("Swift Messages view not found")
        }
        
        SwiftMessages.hideAll()
        
        let messageView = MessageView.viewFromNib(layout: .cardView)
        messageView.button?.isHidden = true
        messageView.titleLabel?.isHidden = true
        messageView.configureContent(body: "No internet connection!")
        messageView.configureTheme(.error)
        
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "InternetConnectionMonitor")
        
        monitor.pathUpdateHandler = { [weak self] pathUpdateHandler in
            guard let self = self else { return }
            if pathUpdateHandler.status == .satisfied {
                DispatchQueue.main.async {
                    self.lessInternetConnectionMessage.hideAll()
                    reachableClosure(.connected)
                }
                
            } else {
                DispatchQueue.main.async {[weak self]  in
                    guard let self = self else { return }
                    self.lessInternetConnectionMessage.show(config: config, view: view)
                    reachableClosure(.disConnected)
                }
            }
        }
        monitor.start(queue: queue)
    }
}

enum connectionStatus {
    case connected
    case disConnected
}
