//
//  PKHUDIndicator.swift
//  CountryFood
//
//  Created by mohamed hashem on 7/1/20.
//  Copyright © 2020 mohamed hashem. All rights reserved.
//

import Foundation
import PKHUD

class PKHUDIndicator {

    class func showProgressView() {
        HUD.hide(afterDelay: 120)
        HUD.show(.systemActivity)
    }
    
    class func hideProgressView() {
        HUD.hide()
    }

    class func hideBySuccessFlash() {
        HUD.flash(.success)
    }

    class func hideByErrorFlash() {
        HUD.flash(.error)
    }
}
