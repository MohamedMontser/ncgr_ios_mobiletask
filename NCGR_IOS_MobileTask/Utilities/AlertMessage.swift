//
//  AlertMessage.swift
//  CountryFood
//
//  Created by mohamed hashem on 7/1/20.
//  Copyright © 2020 mohamed hashem. All rights reserved.
//

import UIKit
import SwiftMessages
import Moya
import Network

class PopUpAlert {

    let alert: UIAlertController

    init(title: String? = nil, message: String? = nil, type: UIAlertController.Style = .alert) {
        alert = UIAlertController(title: title, message: message, preferredStyle: type)
    }

    func add(action: UIAlertAction) -> Self {
        alert.addAction(action)
        return self
    }

    func addCancelAction(title: String = "Cancel") -> Self {
        alert.addAction(UIAlertAction(title: title, style: .cancel, handler: nil))
        return self
    }

    func show(in vc: UIViewController? = nil) {
        if let vc = vc {
            vc.present(alert, animated: true, completion: nil)
        }
    }

    @discardableResult
    class func genericErrorAlert(error: String = "Something went wrong, please try again later") -> PopUpAlert {
        return PopUpAlert(
            title: "oops sorry" ,
            message: error
        ).addCancelAction(title: "Ok" )
    }

    class func showErrorToastWith(message: String = "Something went wrong, please try again later" , _ error: Error? = nil) {

        let messageView = MessageView.viewFromNib(layout: .tabView)
        
        messageView.button?.isHidden = true
        messageView.titleLabel?.isHidden = true
        messageView.configureContent(body: message)
        messageView.configureTheme(.error)
        
        //FIXME: remove hideAll()
        SwiftMessages.hideAll()
        
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.ignoreDuplicates = false
        config.duration = .seconds(seconds: 2)
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: messageView)
        }
    }

    class func showSuccessToastWith(message: String) {
        let messageView = MessageView.viewFromNib(layout: .tabView)

        messageView.button?.isHidden = true
        messageView.titleLabel?.isHidden = true
        messageView.configureContent(body: message)
        messageView.configureTheme(.success)

        //FIXME: remove hideAll()
        SwiftMessages.hideAll()

        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.ignoreDuplicates = false
        config.duration = .seconds(seconds: 2)
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: messageView)
        }
    }

    class func showInfoToastWith(label: String = "please refresh the screen to see the last status of the order." ) {
        let messageView = MessageView.viewFromNib(layout: .cardView)

        messageView.button?.isHidden = true
        messageView.titleLabel?.isHidden = true
        messageView.configureContent(body: label)
        messageView.configureTheme(.info)

        //FIXME: remove hideAll()
        SwiftMessages.hideAll()

        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.ignoreDuplicates = false
        config.duration = .seconds(seconds: 2)
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: messageView)
        }
    }

    class func lessInternetConnection(message: String = "No internet connection!" ) {
        let messageView = MessageView.viewFromNib(layout: .statusLine)

        messageView.button?.isHidden = true
        messageView.titleLabel?.isHidden = true
        messageView.configureContent(body: message)
        messageView.configureTheme(.warning)

        //FIXME: remove hideAll()
        SwiftMessages.hideAll()

        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.ignoreDuplicates = false
        config.duration = .seconds(seconds: 5)
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: messageView)
        }
    }

    class func notPermitted() {
        let messageView = MessageView.viewFromNib(layout: .statusLine)

        messageView.button?.isHidden = true
        messageView.titleLabel?.isHidden = true
        messageView.configureContent(body: "Not Permitted!" )
        messageView.configureTheme(.warning)

        //FIXME: remove hideAll()
        SwiftMessages.hideAll()

        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.ignoreDuplicates = false
        config.duration = .seconds(seconds: 5)
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: messageView)
        }
    }

    struct ErrorModel: Codable {
        var error: String?
    }

    struct UnAuthErrorModel: Codable {
        var message: String?
    }
}
