//
//  Helper.swift
//  NCGR_IOS_MobileTask
//
//  Created by muhammed gamal on 11/16/21.
//  Copyright © 2021 mohamed montser. All rights reserved.
//

import Foundation


class Helper{
    static func getValidDateString(dateString: String) -> String{
        
        let dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)!
        if Calendar.current.isDateInToday(date){
            return "Today"
        }else if Calendar.current.isDateInTomorrow(date){
            return "tomorrow"
        }else{
            dateFormatter.dateFormat = "E d MMM"
            return dateFormatter.string(from: date)
        }
    }
    
}
